<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Python</title>

    <link rel="stylesheet" href="/en/latest/css/core.css">
    <link rel="stylesheet" href="/en/latest/css/getstarted.css">
    <link rel="shortcut icon" href="/en/latest/css/images/saltStack_favIcon_32x32.png">

</head>
<body>

<nav id="globalNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="http://saltstack.com/" target="_blank"><img src="/en/latest/css/images/saltstack.svg" height="40px" width="170px" class="nolightbox"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="nav navbar-nav">
                <li><a href="/en/latest/">Overview</a></li>
                <li><a href="/en/getstarted/">Tutorials</a></li>
                <li><a href="/en/latest/contents.html">Documentation</a></li>
                <li><a href="https://repo.saltstack.com">Downloads</a></li>
                <li><a href="/en/latest/topics/development/">Develop</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="jumbotron">

    <div class="row row-centered">
        <div class="col-md-9 col-centered">
            <h1>Understanding SaltStack</h1>

            <h3>Get Started Tutorial</h3>

        </div>
    </div>
    <!--<div class="gs-top-nav">
        <a href="/en/getstarted/getstarted/index.html">Back to Get Started <i class="glyphicon glyphicon-arrow-left"></i></a><br>
        <a href="/en/getstarted">Back to Documentation <i class="glyphicon glyphicon-arrow-left"></i></a>
    </div>-->
</div>







<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ol class="progtrckr" data-progtrckr-steps="8">
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/index.html">Salt Approach</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/plugins.html">Plug-ins</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/communication.html">Communication</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/execution.html">Remote Execution</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/states.html">States</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/runners.html">Runners</a>
                </li>
                
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/data.html">System Data</a>
                </li>
                
                
                
                <li class="progtrckr-done">Python</li>
                
                
                
                
                
                
                
                
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">

            

            <h1 id="python">Python</h1>
<p>I know that we said earlier that you don’t need to be able to write Python or other code to use Salt, which is true. However, it is useful in many circumstances to be able to <em>read</em> Python, or at least Python function documentation, which turns out is much easier that writing Python.</p>
<p>This section explains the Python basics that help you better understand how Salt works.</p>
<h2 id="modules">Modules</h2>
<div class="section sidebar">
<h4 id="modules-are-used-differently-by-each-subsystem">Modules are used differently by each subsystem!</h4>
<p>If you glanced at the salt source folder, you are probably feeling a bit overwhelmed by Python code about now. Fortunately, there are only two module types that you really need to interact with directly: execution modules (<code>salt.module</code>) and state modules (<code>salt.states</code>).</p>
<p>The modules that let you format job results (<code>salt.outputters</code>) are enabled using the <code>--out</code> argument when calling the <code>salt</code> command. Most of the other subsystem modules are enabled in the Salt master or minion configuration files, including auth, beacons, engines, fileserver, pillar, proxy, returners, and probably one or two others that I missed. And some you don’t have to do anything with, such as grains, since they do their work automatically.</p>
</div>
<p>In Salt, each subsystem plug-in is a Python module. In Salt terms, you can think of a module as a group of functions (aka commands) that manage an application (mysql, docker), a system component (disk, file), or interact with an external system (gitfs).</p>
<p>Here is what you need to know:</p>
<ul>
<li><p>All of the modules are in the <code>salt</code> folder in the source. A separate folder exists for each subsystem, and each module is a separate file ending in <code>.py</code>.</p></li>
<li><p>Modules are namespaced in the format salt.<em>subsystem</em>.<em>module</em>. This namespace is visible in the docs, so you can quickly tell which type of subystem module you are viewing. One point of confusion: execution modules start with <code>salt.module</code> since they were the first and only modules available in the initial versions of Salt (when time travel is invented we should probably start these with <code>salt.execution</code> instead).</p></li>
<li><p>Modules contain as many or as few functions as are needed. The file execution module (<code>salt.modules.file</code>) has a lot of functions because file management is sort of a big deal. The uwsgi stats server execution module (<code>salt.modules.uwsgi</code>) has just one.</p></li>
</ul>
<h2 id="functions">Functions</h2>
<p>If you understood the previous section on modules, functions are even easier. You can think of functions as the specific commands within a module that you can call to manage and configure systems. For example, <code>salt.modules.pkg.install</code>, <code>salt.modules.network.interfaces</code> and <code>salt.modules.user.add</code> are all common execution functions. Functions are the verbs of Salt, and you can usually figure out which function you want to call by opening the module docs and looking at the function names.</p>
<h2 id="arguments">Arguments</h2>
<p>Arguments are a little more complex, especially if you are not familiar with keyword arguments, and argument syntax is where you’ll usually stumble as you are getting started with Salt.</p>
<p>Arguments are expressed differently between remote execution and states, so we’ll cover each separately.</p>
<h3 id="execution-function-arguments">Execution function arguments</h3>
<p>Execution arguments are passed as additional values when calling Salt on the command line, either as just a value, or as <code>argument=value</code>. Required values are usually passed in a specific order as the value only separated by spaces, and optional arguments that have a default value are passed as <code>argument=value</code>.</p>
<p>For example, take a look at the function signature for <code>user.add</code> that appears in the Salt docs (<code>user</code> is a virtual module, so we are looking at the docs for <code>useradd</code>):</p>
<pre class="language-python"><code class="language-python">salt.modules.useradd.add(name, uid=<span class="ot">None</span>, gid=<span class="ot">None</span>, groups=<span class="ot">None</span>, home=<span class="ot">None</span>,
shell=<span class="ot">None</span>, unique=<span class="ot">True</span>, system=<span class="ot">False</span>, fullname=<span class="st">&#39;&#39;</span>, roomnumber=<span class="st">&#39;&#39;</span>,
workphone=<span class="st">&#39;&#39;</span>, homephone=<span class="st">&#39;&#39;</span>, createhome=<span class="ot">True</span>, loginclass=<span class="ot">None</span>)</code></pre>
<p>When calling this module, name is required and you can see that there is not a default value provided. The remaining arguments will use the value listed unless you pass a different value as an override. The following example creates a user named Fred and sets a different default shell:</p>
<pre class="language-bash"><code class="language-bash"><span class="kw">salt</span> <span class="st">&#39;*&#39;</span> user.add fred shell=/bin/zsh</code></pre>
<p>Escaping follows the rules of your shell.</p>
<h4 id="more-examples">More Examples</h4>
<p>Regular arguments and keyword arguments (which are usually shortened to args and kwargs) are the most common types you’ll encounter. Here is an example of passing one arg and three kwargs:</p>
<pre class="language-bash"><code class="language-bash"><span class="kw">salt</span> <span class="st">&#39;*&#39;</span> network.connect google-public-dns-a.google.com port=53 proto=udp timeout=3</code></pre>
<p>Passing two args and one kwarg:</p>
<pre class="language-bash"><code class="language-bash"><span class="kw">salt</span> <span class="st">&#39;*&#39;</span> cp.get_file salt://vimrc /etc/vimrc gzip=5</code></pre>
<p>A few execution modules accept lists, which are passed as follows:</p>
<pre class="language-bash"><code class="language-bash"><span class="kw">`salt</span> ns1 pkg.install pkgs=[<span class="st">&#39;bind9&#39;</span>,<span class="st">&#39;bind9-docs&#39;</span>,<span class="st">&#39;bind-utils&#39;</span>]<span class="kw">`</span></code></pre>
<p>Some even take a list of dictionaries:</p>
<pre class="language-bash"><code class="language-bash"><span class="kw">`salt</span> <span class="st">&#39;*&#39;</span> pkg.install sources=<span class="st">&#39;[{&quot;foo&quot;: &quot;salt://foo.deb&quot;},{&quot;bar&quot;: &quot;salt://bar.deb&quot;}]&#39;</span><span class="kw">`</span></code></pre>
<p>The Salt docs contain a lot of examples, and if you get stuck there are usually friendly Salt users in <a href="http://webchat.freenode.net/?channels=salt&amp;uio=mj10cnvljjk9dhj1zsyxmd10cnvl83">IRC</a> who can help you out.</p>
<h3 id="state-function-arguments">State function arguments</h3>
<div class="section sidebar">
<h4 id="user.add-vs-user.present">user.add vs user.present</h4>
<p>Since state functions are stateful, the <code>salt.states.user.present</code> function checks first to see if the user already exists, and if not, creates the user to satisfy the state of “present”. The <code>salt.modules.user.add</code> execution function simply adds the user, which will probably cause an error if the user already exists.</p>
</div>
<p>State functions are called in state files using YAML syntax. The YAML syntax works great to represent data types, so you’ll probably find that calling functions with complex data types is easier in states. To see how a user is added in a state, lets look at the <code>salt.states.user.present</code> function:</p>
<pre class="language-bash"><code class="language-bash"><span class="kw">salt.states.user.present</span>(name, uid=None, gid=None, gid_from_name=False,
<span class="ot">groups=</span>None, <span class="ot">optional_groups=</span>None, <span class="ot">remove_groups=</span>True, <span class="ot">home=</span>None,
<span class="ot">createhome=</span>True, <span class="ot">password=</span>None, <span class="ot">hash_password=</span>False, <span class="ot">enforce_password=</span>True,
<span class="ot">empty_password=</span>False, <span class="ot">shell=</span>None, <span class="ot">unique=</span>True, <span class="ot">system=</span>False, <span class="ot">fullname=</span>None,
<span class="ot">roomnumber=</span>None, <span class="ot">workphone=</span>None, <span class="ot">homephone=</span>None, <span class="ot">loginclass=</span>None, <span class="ot">date=</span>None,
<span class="ot">mindays=</span>None, <span class="ot">maxdays=</span>None, <span class="ot">inactdays=</span>None, <span class="ot">warndays=</span>None, <span class="ot">expire=</span>None,
<span class="ot">win_homedrive=</span>None, <span class="ot">win_profile=</span>None, <span class="ot">win_logonscript=</span>None,
<span class="ot">win_description=</span>None)</code></pre>
<p>Even though there are a lot of arguments, calling functions with keyword arguments in YAML is easy:</p>
<pre class="language-yaml"><code class="language-yaml"><span class="fu">a state example that calls user.present:</span>
  <span class="fu">user.present:</span>
    <span class="kw">-</span> <span class="fu">name:</span> fred
    <span class="kw">-</span> <span class="fu">shell:</span> /bin/zsh</code></pre>
<p>Lists are also easy to define using YAML:</p>
<pre class="language-yaml"><code class="language-yaml"><span class="fu">install bind packages:</span>
  <span class="fu">pkg.installed:</span>
    <span class="kw">-</span> <span class="fu">pkgs:</span>
      <span class="kw">-</span> bind9
      <span class="kw">-</span> bind9-docs
      <span class="kw">-</span> bind-utils</code></pre>
<p>Defining a list of dictionaries is also easier than on the command line:</p>
<pre class="language-yaml"><code class="language-yaml"><span class="fu">Install some packages using other sources:</span>
  <span class="fu">pkg.installed:</span>
    <span class="kw">-</span> <span class="fu">name:</span> mypkgs
    <span class="kw">-</span> <span class="fu">sources:</span>
      <span class="kw">-</span> <span class="fu">foo:</span> salt://foo.deb
      <span class="kw">-</span> <span class="fu">bar:</span> http://somesite.org/bar.deb</code></pre>
<p>One more tip that applies to both execution and state functions: not all available arguments are listed in the function signature. Several execution functions act as wrappers to other Python APIs or interfaces, so they just pass along any arguments that you provide. These additional arguments are usually documented below the signature, but sometimes you’ll have to check the code or the corresponding interface to know exactly what is supported.</p>
<p>This should give you enough understanding of how Python is used by the execution and state subsystems to get you started. Continue to <em>SaltStack Fundamentals</em> to learn how to install and start using Salt.</p>


            <br>
            
            <a href="/en/getstarted/system/data.html">
                <button type="button" class="btn btn-secondary"><span
                        class="glyphicon glyphicon-chevron-left"></span> Previous
                </button>
            </a>
            

            
            <a href="/en/getstarted/fundamentals/">
                <button type="button" class="btn btn-primary btn-right">
                    <span class="glyphicon glyphicon-chevron-right"></span> Continue to SaltStack Fundamentals
                </button>
            </a>
            

        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="footer">
  <p>© 2020 SaltStack. All Rights Reserved, SaltStack Inc. | <a href="http://saltstack.com/privacy-policy" target="_blank">Privacy Policy</a></p>
</div>
</div>

<script type="text/javascript" src="/en/latest/js/core-min.js"></script>



<!--analytics-->
<script type="text/javascript">llactid=23943</script>
<script type="text/javascript" src="https://trackalyzer.com/trackalyze_secure.js"></script>

<script>
          var _gaq = _gaq || [];
          var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
          _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
          _gaq.push(['_setAccount', 'UA-26984928-1']);
          _gaq.push(['_setDomainName', 'saltstack.com']);
          _gaq.push(['_trackPageview']);

          (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();

</script>

</body>
</html>