<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Plug-ins</title>

    <link rel="stylesheet" href="/en/latestcss/core.css">
    <link rel="stylesheet" href="/en/latestcss/getstarted.css">
    <link rel="shortcut icon" href="/en/latestcss/images/saltStack_favIcon_32x32.png">

</head>
<body>

<nav id="globalNavbar" class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="http://saltstack.com/" target="_blank"><img src="/en/latestcss/images/saltstack.svg" height="40px" width="170px" class="nolightbox"></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="nav navbar-nav">
                <li><a href="/en/latest">Documentation</a></li>
                <li><a href="/en/getstarted/">Tutorials</a></li>
                <li><a href="/en/latestcontents.html">Reference Guide</a></li>
                <li><a href="https://repo.saltstack.com">Downloads</a></li>
                <li><a href="/en/latesttopics/development/">Develop</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="jumbotron">

    <div class="row row-centered">
        <div class="col-md-9 col-centered">
            <h1>Understanding SaltStack</h1>

            <h3>Get Started Tutorial</h3>

        </div>
    </div>
    <!--<div class="gs-top-nav">
        <a href="/en/getstarted/getstarted/index.html">Back to Get Started <i class="glyphicon glyphicon-arrow-left"></i></a><br>
        <a href="/en/getstarted">Back to Documentation <i class="glyphicon glyphicon-arrow-left"></i></a>
    </div>-->
</div>







<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ol class="progtrckr" data-progtrckr-steps="8">
                
                
                
                <li class="progtrckr-done">
                    
                    <a href="/en/getstarted/system/index.html">Salt Approach</a>
                </li>
                
                
                
                <li class="progtrckr-done">Plug-ins</li>
                
                
                
                
                
                
                
                
                
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/communication.html">Communication</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/execution.html">Remote Execution</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/states.html">States</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/runners.html">Runners</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/data.html">System Data</a>
                </li>
                
                
                
                
                <li class="progtrckr-todo">
                    
                    <a href="/en/getstarted/system/python.html">Python</a>
                </li>
                
                
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">

            

            <h1 id="salt-plug-ins">Salt Plug-ins</h1>
<p>Any discussion about the Salt approach would be incomplete without a discussion of plug-ins. Understanding plug-ins and Salt’s pluggable architecture is often the “<a href="https://en.wikipedia.org/wiki/Eureka_(word)">eureka</a>” moment that turns Salt investigators into Salt evangelists.</p>
<p>The basic explanation is this: Salt’s core framework provides a high-speed communication and event bus. This framework connects and authenticates your managed systems, and provides a way for these systems to send notifications.</p>
<p>On top of this core framework, the remaining features of Salt are exposed as a set of loosely coupled, pluggable subsystems.</p>
<h2 id="pluggable-subsystems">Pluggable Subsystems</h2>
<p>Salt contains over 20 pluggable subsystems, but most users are interested in only the handful that are used to directly manage systems. The following table contains a list of some of the more common subsystems in Salt:</p>
<table class="table">
<thead>
<tr class="header">
<th align="left">Subsystem</th>
<th align="left"></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">Authentication</td>
<td align="left">Authorizes a user before running a job.</td>
</tr>
<tr class="even">
<td align="left">File server</td>
<td align="left">Distributes files.</td>
</tr>
<tr class="odd">
<td align="left">Secure data store</td>
<td align="left">Makes user-defined variables and other data securely available.</td>
</tr>
<tr class="even">
<td align="left">State representation</td>
<td align="left">Describes your infrastructure and system configurations.</td>
</tr>
<tr class="odd">
<td align="left">Return formatter</td>
<td align="left">Formats job results into a normalized data structure.</td>
</tr>
<tr class="even">
<td align="left">Result cache</td>
<td align="left">Sends job results to long-term storage.</td>
</tr>
<tr class="odd">
<td align="left">Remote execution</td>
<td align="left">Runs a wide variety of tasks to install software, distribute files, and other things you need to do to manage systems.</td>
</tr>
<tr class="even">
<td align="left">Configuration</td>
<td align="left">Configures targeted systems to match a desired state.</td>
</tr>
</tbody>
</table>
<div class="section sidebar">
<h4 id="what-does-it-mean-to-be-pluggable">What does it mean to be “pluggable”?</h4>
<p>Salt doesn’t define a built-in way to perform the task for any subsystem, each subsystem simply delegates its work to a plug-in. Salt provides a default plug-in for each system, but changing the plug-in in most cases is as simple as updating a configuration file. This pluggability is what makes Salt so flexibile.</p>
</div>
<p>To illustrate, the following diagram shows several of the common subsystems along with the most common plug-ins for each subsystem.</p>
<p><img width="50%" class="imgcenter" src="/en/getstarted/images/salt-subsystems.png"></p>
<p>This diagram shows only a handful of the available subsystems and plug-ins, but it should give you a sense of the general architecture of Salt.</p>
<h2 id="subsystems-during-a-job-run">Subsystems during a Job Run</h2>
<p>When a job runs, several Salt subsystems are invoked in order to process the job. The following diagram shows the subsystem flow of a typical state run or remote execution job:</p>
<p><img width="80%" class="imgcenter" src="/en/getstarted/images/salt-subsystems-workflow.png"></p>
<div class="section sidebar">
<h4 id="plug-ins-it-sounds-like-you-are-talking-about-salt-modules">Plug-ins? It sounds like you are talking about Salt Modules!</h4>
<p>In Salt, plug-ins are known by their Python name, <em>module</em>. Since each plug-in is a Python module, most of the time they are simply called modules, or more accurately, Salt <em>subsystem</em> modules (Salt auth modules, Salt fileserver modules, and so on).</p>
<p>As long as you understand that each Salt module is a plug-in that extends one of the many subsystems in Salt, you’ll understand this relationship well enough.</p>
</div>
<p>At each step, the subsystem delegates its work to the configured plug-in. For example, the job returner plug-in in step 7 might be one of 30 plug-ins including MySQL, reddis, or not be configured at all (the job returner plug-in can also run directly on the managed system after step 4).</p>
<p>At each step, there are many plug-ins available to perform a task, resulting in hundreds of possible Salt configurations and workflows.</p>
<h2 id="flexibility">Flexibility</h2>
<p>This flexibility makes salt an extremely powerful and customizable tool, but it also makes it a bit hard to answer standard questions when you are learning about the tool.</p>
<p>For fun, let’s take the “technically accurate” approach and answer a few common questions about Salt:</p>
<ul>
<li><p><em>How do you start Salt jobs?</em> - From any interface that can call Python, a REST API, the command line, or by using Salt’s built-in scheduler.</p></li>
<li><p><em>How does salt format results?</em> - YAML, JSON, plain text, python data structure, and several other formats, and you can change the format anytime using a single argument.</p></li>
<li><p><em>What format does Salt use for configuration declarations?</em> - Choose from one of 15 supported formats depending on your use case, and you also have your choice of templating languages. The format is specified on a per file basis, so you can use multiple formats simultaneously.</p></li>
<li><p><em>Where are results stored?</em> Anywhere you want, you have 30 options!</p></li>
</ul>
<p>Aside from making you very annoying in conversation, answering questions this way does say something about the Salt approach to management. You know your infrastructure, and in today’s complex environments there isn’t a single best way to do anything.</p>
<p>Don’t let this worry you though, Salt comes out-of-the-box with excellent defaults that are used by most Salt users. The main point is that the flexibility is there when and if you need it.</p>
<h2 id="salt-components">Salt Components</h2>
<p>With your new knowledge of the pluggable subsystems in Salt, hopefully you are starting to understand that each Salt component is actually a pluggable subsystem in Salt, with corresponding plug-ins. Salt grains? Salt pillar? Salt runners? All pluggable subsystems that are easily extended.</p>
<h2 id="virtual-modules">Virtual Modules</h2>
<p>We’ve covered a lot here, but there is one additional thing we need to tackle regarding modules. Remember earlier when we explained how Salt abstracts the underlying details of the operating system? One way that Salt achieves this abstraction is by using virtual modules.</p>
<p>Some management tasks are performed so differently between operating systems, there is very little code that can be reused between them when writing a plug-in.</p>
<p>For example, package management on Debian systems is done using an execution module called <code>aptpkg</code>. On Redhat, it is done using an execution module called <code>yumpkg</code> (for reasons that should be apparent). If you’ve used Salt at all though, you’d know that salt calls the <code>pkg</code> remote execution module for package management and it works across all OSs.</p>
<p>To enable this type of abstraction, Salt loads these types of modules using a virtual module name. The <code>aptpkg</code> module contains instructions that basically say “If you are a Debian system, load this module as <code>pkg</code>. Otherwise, don’t load this!” Similar code is present in the <code>yumpkg</code> with a check for Redhat or CentOS. This way multiple modules can exist to perform the same task, but only one gets loaded with the virtual name.</p>
<p>Keep this in mind when you are reading module documentation, as you often need to read the documentation for the non-virtual module to understand the behavior. You can search for <code>__virtualname__</code> inside a Salt module to find out the name that Salt uses when loading that module.</p>
<p>Still with me? Good. Let’s move on and talk about how Salt communicates on the wire.</p>


            <br>
            
            <a href="/en/getstarted/system/index.html">
                <button type="button" class="btn btn-secondary"><span
                        class="glyphicon glyphicon-chevron-left"></span> Previous
                </button>
            </a>
            

            
            <a href="/en/getstarted/system/communication.html">
                <button type="button" class="btn btn-primary btn-right">
                    Next <span class="glyphicon glyphicon-chevron-right"></span></button>
            </a>
            

        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="footer">
    <p>© 2020 SaltStack | <a href="http://saltstack.com/privacy-policy" target="_blank">Privacy Policy</a></p>
</div>
</div>

<script type="text/javascript" src="/en/latestjs/core-min.js"></script>



<!--analytics-->
<script type="text/javascript" language="javascript">llactid=23943</script>
<script type="text/javascript" language="javascript" src="https://trackalyzer.com/trackalyze_secure.js"></script>

<script>
          var _gaq = _gaq || [];
          var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
          _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
          _gaq.push(['_setAccount', 'UA-26984928-1']);
          _gaq.push(['_setDomainName', 'saltstack.com']);
          _gaq.push(['_trackPageview']);

          (function() {
              var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
              ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();

</script>

</body>
</html>