# Salt Getting Started Guide

This repo is going to be deprecated or repurposed.
If anything changes or lessons are learned about contributing to this repo, note them in this file.

## Archive note from Alyssa

In my original attempt at deprecating this content, I moved all the contents of the old Getting Started Guide into an archived content folder.
The result was that the pipeline didn't pass because Acrylamid failed to build.
I don't have the time to invest in understanding Acrylamid because we're moving away from this technology.

In light of that, I made a clean copy of the old Getting Started Guide and its complete file system in the folder `archive-copy` in the root directory.
Then, I made changes to each page of content indicating the guide was deprecated and that people should use the Salt User Guide instead.

I also had no idea whether the content in the `content` folder is what shows up on the live site or whether it's the content in the `website-copy` folder.
So far, I'm just making the changes in the `content` folder and we'll see how it goes.
