---
title: Salt Cloud Map
permalink: cloud/map.html
type: page
layout: getstarted.tmpl
series: Salt Cloud
step: 4
overview:
  goals:
    - Goal
    - Goal
  time: 10
  difficulty: 1
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
