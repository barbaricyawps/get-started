---
title: Event Reactor
permalink: event/reactor.html
type: page
layout: getstarted.tmpl
series: Event-Driven Infrastructure
step: 5
overview:
  goals:
    - Configure Salt reactors
    - Understand reactor SLS file syntax
    - React to an event
  time: 10
  difficulty: 3
parent:
  title: Get Started
  url: index.html
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
