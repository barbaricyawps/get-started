---
title: Beacons
permalink: event/beacons.html
type: page
layout: getstarted.tmpl
series: Event-Driven Infrastructure
step: 4
overview:
  goals:
    - Understand Salt beacons
    - Use a Salt beacon to monitor a file or system process
  time: 10
  difficulty: 2
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
