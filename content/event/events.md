---
title: Watching Events
permalink: event/events.html
type: page
layout: getstarted.tmpl
series: Event-Driven Infrastructure
step: 2
overview:
  goals:
    - Understand the event system basics
    - Monitor events
    - Understand event format and data
  time: 10
  difficulty: 1
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
