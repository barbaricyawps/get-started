---
title: Targeting
permalink: fundamentals/targeting.html
type: page
layout: getstarted.tmpl
series: SaltStack Fundamentals
step: 4
overview:
  goals:
    - Define a target to select one or more systems.
    - Use system data to define a target.
    - Use custom, user-defined data to define a target.
  time: 10
  difficulty: 2
modals:
  - id: globbing-modal
    title: Globbing?
    message: |
      Globbing lets you match one or more Salt minion IDs using a wildcard character.
      Asterisk (**\***) matches one or more characters, and question mark (**?**) matches a single character.

      This is useful if your Salt minion IDs are configured according to function, for example,
      web1, web2, db1, db2, and so on.
    button: Easy!
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
