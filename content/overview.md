---
title: SaltStack Components
type: page
layout: getstarted-intro.tmpl
permalink: overview.html
parent:
  title: Get Started
  url: index.html
---

# Note

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
