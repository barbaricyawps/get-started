---
title: Jinja
permalink: config/jinja.html
type: page
layout: getstarted.tmpl
series: SaltStack Configuration Management
step: 6
overview:
  goals:
    - Use Jinja conditionals
    - Use Jinja loops
    - Call salt execution modules using Jinja
  time: 15
  difficulty: 3
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
