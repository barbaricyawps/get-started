---
title: Pillar
permalink: config/pillar.html
type: page
layout: getstarted.tmpl
series: SaltStack Configuration Management
step: 3
overview:
  goals:
    - Create the pillar Top.sls file
    - Define and assign some pillar keys to your systems
  time: 15
  difficulty: 1
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
