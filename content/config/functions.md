---
title: Functions
permalink: config/functions.html
type: page
layout: getstarted.tmpl
series: SaltStack Configuration Management
step: 2
overview:
  goals:
    - Call Salt state functions
    - Understand the difference between Salt state and Salt execution functions
  time: 15
  difficulty: 1
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
