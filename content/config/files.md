---
title: Manage Files
permalink: config/files.html
type: page
layout: getstarted.tmpl
series: SaltStack Configuration Management
step: 7
overview:
  goals:
    - Deliver files and folders to manages systems
    - Understand Salt file templates
  time: 10
  difficulty: 1
parent:
  title: Get Started
  url: index.html
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
