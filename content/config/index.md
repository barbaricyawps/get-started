---
title: Introduction
permalink: config/index.html
type: page
layout: getstarted.tmpl
series: SaltStack Configuration Management
step: 1
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
