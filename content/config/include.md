---
title: Includes
permalink: config/include.html
type: page
layout: getstarted.tmpl
series: SaltStack Configuration Management
step: 4
overview:
  goals:
    - Reuse Salt states
    - Understand when to use includes
  time: 5
  difficulty: 1
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
