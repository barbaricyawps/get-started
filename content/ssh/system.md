---
title: Install Salt SSH
permalink: ssh/system.html
type: page
layout: getstarted.tmpl
series: Agentless Salt
step: 2
overview:
  goals:
    - Create a Salt roster file with connection details
    - Connect to a remote system and run a command
  time: 10
  difficulty: 1
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
