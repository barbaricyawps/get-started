---
title: Connect
permalink: ssh/connect.html
type: page
layout: getstarted.tmpl
series: Agentless Salt
step: 3
overview:
  goals:
    - Create a Salt roster file with connection details
    - Connect to a remote system and run a command
  time: 10
  difficulty: 2
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
