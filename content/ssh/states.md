---
title: Apply States
permalink: ssh/states.html
type: page
layout: getstarted.tmpl
series: Agentless Salt
step: 4
overview:
  goals:
    - Apply a Salt state using Salt SSH
  time: 10
  difficulty: 2
parent:
  title: Get Started
  url: index.html
---

This guide has been deprecated. Going forward, see the <a href="https://docs.saltproject.io/salt/user-guide/en/latest/">Salt User Guide</a> instead.
